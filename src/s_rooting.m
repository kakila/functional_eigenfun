## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

N  = 100;
x  = NA(N, 1);
t  = linspace (0, 10, N).';
dt = t(2) - t(1);

# g is polynomial
p = [2e-2 -1e-1 -2e-1 1];
g =@(x) polyval (p, x);

# functional
func   =@(g,z) arrayfun (@(T)quadgk (g,t(1),T),z);
func_h =@(g,n,dt) cumsum (g(1:n)) * dt;

l = -1;
x(1) = 0.5;
for i=2:N
  x(i) = fzero (@(z)dt * g(z)-l*(z-x(i-1)), x(i-1));
endfor

y = lsode (@(z,u)g(z)/l, x(1), t);

figure (1)
clf
z = linspace (min(x), max(x), N).';
h = plot (z, g(z), x, zeros(N,1), '.r');
set (h(2),'markersize', 9);
axis tight
grid on

figure(2)
clf
plot (t, func_h(g(x), N, dt), t(2:end), l*x(2:end),'o')
axis tight
grid on

figure(3)
clf
plot (t, x,'o', t, y)
axis tight
grid on
